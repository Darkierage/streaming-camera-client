package com.streaming.camera.controllers;

import com.streaming.camera.Client;
import com.streaming.camera.handlers.StreamFrameListener;
import com.streaming.camera.ui.LoginPanel;
import com.streaming.camera.ui.StartPanel;
import com.streaming.camera.ui.VPanel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.image.BufferedImage;
import java.net.InetSocketAddress;

public class StreamController {
    private final static Logger logger = LoggerFactory.getLogger(StreamController.class);

    private final Client client;
    private static VPanel vPanel;
    private static StartPanel startPanel;
    private static LoginPanel loginPanel;

    public StreamController(Client client) {
        this.client = client;
    }

    public void setVideoPanel(VPanel vPanel) {
        this.vPanel = vPanel;
    }

    public void setStartPanel(StartPanel startPanel) {
        this.startPanel = startPanel;
    }

    public void setLoginPanel(LoginPanel loginPanel) {
        this.loginPanel = loginPanel;
    }

    public void register(String username, String password) {
        //mock
    }

    public void loginClick() {
        startPanel.setVisible(false);
        loginPanel.setVisible(true);
    }

    public void login(String username, String password) {
        loginPanel.setVisible(false);
        vPanel.setVisible(true);
        client.connect(new InetSocketAddress("192.168.0.241", 20000));
        //mock
    }

    public void stopStream() {
        client.stop();
    }

    public void moveLeft() {
        client.moveLeft();
    }

    public void moveRight() {
        client.moveRight();
    }

    public static class StreamFrameListenerIMPL implements StreamFrameListener {
        private volatile long count = 0;

        @Override
        public void onFrameReceived(BufferedImage image) {
            logger.info("frame received :{}", count++);
            vPanel.updateImage(image);
        }
    }
}
