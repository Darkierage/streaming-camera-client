package com.streaming.camera.ui;

import com.streaming.camera.controllers.StreamController;

import javax.swing.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class StartPanel {
    private JButton login;
    private JButton register;

    public JPanel panel;
    private StreamController sc;

    private final ExecutorService worker = Executors.newSingleThreadExecutor();

    public StartPanel(StreamController sc) {
        this.sc = sc;

        login.addActionListener(e -> worker.execute(sc::loginClick));
    }

    public void setVisible(boolean isVisible) {
        panel.setVisible(isVisible);
    }

    private void createUIComponents() {
        panel = new JPanel();
    }
}
