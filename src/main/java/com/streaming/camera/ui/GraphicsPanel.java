package com.streaming.camera.ui;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class GraphicsPanel extends JPanel {

    private BufferedImage image;
    private final ExecutorService worker = Executors.newSingleThreadExecutor();

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(image, 0, 0, null);
    }

    void updateImage(BufferedImage uimage) {
        worker.execute(() -> {
            image = uimage;
            repaint();
        });
    }
}