package com.streaming.camera.ui;

import com.streaming.camera.controllers.StreamController;

import javax.swing.*;
import java.awt.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class LoginPanel {
    private JTextArea loginText;
    private JTextArea passwordText;
    private JButton loginButton;
    private JLabel loginLabel;
    private JLabel passwordLabel;

    public JPanel panel;
    private StreamController sc;

    private final ExecutorService worker = Executors.newSingleThreadExecutor();

    public LoginPanel(StreamController sc) {
        this.sc = sc;

        loginButton.addActionListener(e -> worker.execute(() -> sc.login("username", "password")));
    }

    public void setVisible(boolean isVisible) {
        panel.setVisible(isVisible);
    }

    private void createUIComponents() {
        loginButton = new JButton();
        panel = new JPanel();
    }
}
