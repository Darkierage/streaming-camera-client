package com.streaming.camera.ui;

import com.streaming.camera.controllers.StreamController;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class VPanel {
    public JPanel panel;
    private GraphicsPanel graphicsPanel;
    private JButton moveLeft;
    private JButton moveRight;

    private StreamController sc;
    private final ExecutorService worker = Executors.newSingleThreadExecutor();

    public VPanel(StreamController sc) {
        this.sc = sc;

        moveLeft.addActionListener(e -> worker.execute(sc::moveLeft));
        moveRight.addActionListener(e -> worker.execute(sc::moveRight));
    }

    public void setVisible(boolean isVisible) {
        panel.setVisible(isVisible);
    }

    private void createUIComponents() {
        panel = new JPanel();
        graphicsPanel = new GraphicsPanel();
        moveLeft = new JButton();
        moveRight = new JButton();
    }

    public void updateImage(BufferedImage image) {
        graphicsPanel.updateImage(image);
    }
}
