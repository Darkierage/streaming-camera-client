package com.streaming.camera;

import com.streaming.camera.controllers.StreamController;
import com.streaming.camera.ui.*;
import com.sun.java.swing.plaf.windows.WindowsLookAndFeel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class App {
    private final static Dimension dimension = new Dimension(320, 240);

    public static void main(String[] args) {
        Client client = new Client(new StreamController.StreamFrameListenerIMPL(), dimension);
        StreamController sc = new StreamController(client);
        InitUI(sc);
    }

    private static void InitUI(StreamController sc) {
        try {
            UIManager.setLookAndFeel(new WindowsLookAndFeel());
        } catch (Exception e) {
            e.printStackTrace();
        }

        JPanel panelContainer = new JPanel();

        VPanel vPanel = new VPanel(sc);
        sc.setVideoPanel(vPanel);

        StartPanel startPanel = new StartPanel(sc);
        sc.setStartPanel(startPanel);

        LoginPanel loginPanel = new LoginPanel(sc);
        sc.setLoginPanel(loginPanel);

        panelContainer.add(vPanel.panel);
        panelContainer.add(startPanel.panel);
        panelContainer.add(loginPanel.panel);

        JFrame frame = new JFrame("streaming-camera");
        frame.setContentPane(panelContainer);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent e)
            {
                sc.stopStream();
                e.getWindow().dispose();
            }
        });
    }
}