package com.streaming.camera.handlers;

import java.awt.image.BufferedImage;

public interface StreamFrameListener {
	void onFrameReceived(BufferedImage image);
}
