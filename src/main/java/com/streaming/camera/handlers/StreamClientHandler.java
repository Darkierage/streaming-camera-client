package com.streaming.camera.handlers;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;

public class StreamClientHandler extends SimpleChannelHandler {
    private final StreamClientListener streamClientListener;
    private final static Logger logger = LoggerFactory.getLogger(StreamClientHandler.class);

    public StreamClientHandler(StreamClientListener streamClientListener) {
        super();
        this.streamClientListener = streamClientListener;
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) {
        Channel channel = e.getChannel();
        Throwable t = e.getCause();
        logger.debug("exception at :{}", channel);
        streamClientListener.onException(channel, t);
        //super.exceptionCaught(ctx, e);
    }

    @Override
    public void channelConnected(ChannelHandlerContext ctx, ChannelStateEvent e)
            throws Exception {
        Channel channel = e.getChannel();
        logger.info("com.streaming.camera.channel connected at {}", channel);
        streamClientListener.onConnected(channel);
        super.channelConnected(ctx, e);
    }

    @Override
    public void channelDisconnected(ChannelHandlerContext ctx,
                                    ChannelStateEvent e) throws Exception {
        Channel channel = e.getChannel();
        logger.info("com.streaming.camera.channel disconnected at :{}", channel);
        streamClientListener.onDisconnected(channel);
        super.channelDisconnected(ctx, e);
    }

    @Override
    public void messageReceived(ChannelHandlerContext ctx, MessageEvent e)
            throws Exception {
        ChannelBuffer channelBuffer = (ChannelBuffer) e.getMessage();
        logger.info("message received :{}", channelBuffer.readableBytes());
        super.messageReceived(ctx, e);
    }

    @Override
    public void writeComplete(ChannelHandlerContext ctx, WriteCompletionEvent e)
            throws Exception {
        super.writeComplete(ctx, e);
    }
}