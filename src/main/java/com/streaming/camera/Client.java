package com.streaming.camera;

import com.streaming.camera.channel.StreamClientChannelPipelineFactory;
import com.streaming.camera.handlers.StreamClientListener;
import com.streaming.camera.handlers.StreamFrameListener;
import org.jboss.netty.bootstrap.ClientBootstrap;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.socket.nio.NioClientSocketChannelFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.net.SocketAddress;
import java.util.concurrent.Executors;

public class Client {
    private final static Logger logger = LoggerFactory.getLogger(Client.class);

    private final ClientBootstrap clientBootstrap;
    private Channel clientChannel;

    Client(StreamFrameListener streamFrameListener,
           Dimension dimension) {
        super();

        this.clientBootstrap = new ClientBootstrap();
        this.clientBootstrap.setFactory(new NioClientSocketChannelFactory(
                Executors.newCachedThreadPool(),
                Executors.newCachedThreadPool()));
        this.clientBootstrap.setPipelineFactory(
                new StreamClientChannelPipelineFactory(
                        new StreamClientListenerIMPL(),
                        streamFrameListener,
                        dimension));
    }

    public void connect(SocketAddress streamServerAddress) {
        logger.info("going to connect to stream server :{}", streamServerAddress);
        clientBootstrap.connect(streamServerAddress);
    }

    public void stop() {
        if (clientChannel != null && clientChannel.isConnected()) {
            clientChannel.close();
            clientBootstrap.releaseExternalResources();
        }
    }

    public void moveLeft() {
        clientChannel.write(ChannelBuffers.wrappedBuffer(new byte[]{1}));
    }

    public void moveRight() {
        clientChannel.write(ChannelBuffers.wrappedBuffer(new byte[]{2}));
    }

    protected class StreamClientListenerIMPL implements StreamClientListener {

        @Override
        public void onConnected(Channel channel) {
            //	logger.info("stream connected to server at :{}",com.streaming.camera.channel);
            clientChannel = channel;

//            byte[] test = new byte[]{1};
//            clientChannel.write(ChannelBuffers.wrappedBuffer(test));
        }

        @Override
        public void onDisconnected(Channel channel) {
            //	logger.info("stream disconnected to server at :{}",com.streaming.camera.channel);
        }

        @Override
        public void onException(Channel channel, Throwable t) {
            //	logger.debug("exception at :{},exception :{}",com.streaming.camera.channel,t);
        }
    }
}